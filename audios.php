<?php

	include_once 'db.php';

	class Audio extends DB{
	    
	    function obtenerAudios(){
	        $query = $this->connect()->query('SELECT * FROM call_recording');
	        return $query;
	    }

	    function obtenerAudio($uniqueid){
	        $query = $this->connect()->prepare('SELECT * FROM call_recording WHERE uniqueid = :uniqueid');
	        $query->execute(['uniqueid' => $uniqueid]);
	        return $query;
	    }
	}

	class AudioOld extends DB_old{
	    
	    function obtenerAudiosOld(){
	        $query = $this->connect()->query('SELECT DATE_FORMAT(calldate,"%Y/%m/%d") AS FECHA,dst AS ANEXO,SUBSTRING(dcontext,1,5) AS AGENT,uniqueid AS UNIQUEID FROM cdr');
	        return $query;
	    }

	    function obtenerAudioOld($uniqueid){
	        $query = $this->connect()->prepare("SELECT DATE_FORMAT(calldate,'%Y/%m/%d') AS FECHA,dst AS ANEXO,SUBSTRING(dcontext,1,5) AS AGENT,uniqueid,REPLACE(uniqueid,'.','-') AS UNIQUEID FROM cdr WHERE uniqueid = :uniqueid");
	        $query->execute(['uniqueid' => $uniqueid]);
	        return $query;
	    }
	}
?>