<?php
    include_once 'audios.php';

    class ApiAudios{
        function getAll(){
            $audio = new Audio();
            $audios = array();
            $audios["Grabacion"] = array();
            $res = $audio->obtenerAudios();
            if($res->rowCount()){
                while ($row = $res->fetch(PDO::FETCH_ASSOC)){

                    $link = "https://172.16.96.253/grabaciones/";
                    $audioPath = $row['recordingfile'];
                    $audioLink = $link . $audioPath;

                    $item=array(
                        "uniqueid" => $row['uniqueid'],
                        "audio" => $audioLink,
                    );
                    array_push($audios["Grabacion"], $item);
                }
            
                $this->printJSON($audios);
            }else{
                echo json_encode(array('mensaje' => 'No hay elementos'));
            }
        }

        function getById($uniqueid){
            $audio = new Audio();
            $audios = array();
            $audios["Grabacion"] = array();

            $res = $audio->obtenerAudio($uniqueid);

            if($res->rowCount() == 1){
                $row = $res->fetch();
                
                $link = "https://172.16.96.253/grabaciones/";
                $audioPath = $row['recordingfile'];
                $audioLink = $link . $audioPath;

                $item=array(
                    "uniqueid" => $row['uniqueid'],
                    "audio" => $audioLink,
                );
                array_push($audios["Grabacion"], $item);
                $this->printJSON($audios);
            }else{
                $audio = new AudioOld();
                $audios = array();
                $audios["Grabacion"] = array();
                $res = $audio->obtenerAudioOld($uniqueid);

                if($res->rowCount() == 1){
                    $row = $res->fetch();
                    
                    $link = "http://172.16.96.2/GrabacionesRT/";
                    $fecha = $row['FECHA'];
                    $agente = $row['AGENT'];
                    $anexo = $row['ANEXO'];
                    $idUnique = $row['UNIQUEID'];
                    $audioLink = $link . $fecha . "/" . $agente . "-". $anexo . "-" . $idUnique . ".wav";

                    $item=array(
                        "uniqueid" => $row['uniqueid'],
                        "audio" => $audioLink,
                    );
                    array_push($audios["Grabacion"], $item);
                    $this->printJSON($audios);
                }else{
                    echo json_encode(array('mensaje' => 'No hay elementos'));
                }
            }
        }
        function error($mensaje){
            echo json_encode(array('mensaje' => $mensaje)); 
        }
        function printJSON($array){
            echo '<code>'.json_encode($array).'</code>';
        }
    }
?>